/*Funcion que valida RUT*/
function validaRut(campo) {
    if (campo.length == 0) { return false; }
    if (campo.length < 8) { return false; }

    campo = campo.replace('-', '')
    campo = campo.replace(/\./g, '')

    var suma = 0;
    var caracteres = "1234567890kK";
    var contador = 0;

    for (var i = 0; i < campo.length; i++) {
        u = campo.substring(i, i + 1);

        if (caracteres.indexOf(u) != -1)
            contador++;
    }
    if (contador == 0) { return false }

    var rut = campo.substring(0, campo.length - 1)
    var drut = campo.substring(campo.length - 1)
    var dvr = '0';
    var mul = 2;

    for (i = rut.length - 1; i >= 0; i--) {
        suma = suma + rut.charAt(i) * mul
        if (mul == 7) mul = 2
        else mul++
    }
    res = suma % 11
    if (res == 1) dvr = 'k'
    else if (res == 0) dvr = '0'
    else {
        dvi = 11 - res
        dvr = dvi + ""
    }
    if (dvr != drut.toLowerCase()) { return false; }
    else { return true; }
}
/*Agregamos el metodo validar rut a jquery validate */
jQuery.validator.addMethod("rut", function (value, element) {
    return this.optional(element) || validaRut(value);
}, "RUN no válido, por favor revisar");

/*Agregamos el metodo validar la fecha en el datepicker a jquery validate*/
$.validator.addMethod("maxDate", function (value, element) {
    console.log(value);
    var maxDate = new Date();
    maxDate = maxDate.setFullYear(maxDate.getFullYear() - 18)
    var inputDate = new Date(value);
    if (inputDate < (maxDate))
        return true;
    return false;
}, "Fecha no válida");

/*Agregamos un metodo para validar el telefono a jquery validate*/
jQuery.validator.addMethod("telefono", function (numeroTelefono, element) {
    numeroTelefono = numeroTelefono.replace(/\s+/g, "");
    return this.optional(element) || numeroTelefono.length > 9 &&
    numeroTelefono.match(/^(\+?56)?(\s?)(0?9)(\s?)[9876543]\d{7}$/);
}, "Ingrese un numero válido, ejemplo: +56111111111");

/*Agregamos las reglas que queremos en validate*/
$("#formulario1").validate({

    submitHandler: function (form) {
        formularioEnviado();
    },

    rules: {
        email: {
            required: true,
            email: true
        },
        run: {
            required: true,
            rut: true
        },
        nombre: {
            required: true,
            lettersonly: true

        },
        fechanac: {
            maxDate: true
        },
        fono: {
            telefono: true
        }
    },
    messages: {
        email: {
            required: "Ingresa un email",
            email: "El email no es válido"
        },
        run: {
            required: "Ingresa tu run",
            run: "El run no es valido"
        },
        nombre: {
            required: "Ingrese su nombre completo",
            lettersonly: "Ingrese solo letras"

        },
        fechanac: {
            maxDate: "Debes ser mayor de 18 años"
        },
        fono:{
            telefono:"Ingrese un numero válido, ejemplo: +56111111111"
        }
    }
});

/*Funcion que muestra un mensaje si el formulario fue enviado correctamente*/
function formularioEnviado() {
    var element = document.getElementById("formulario1");
    element.classList.add("d-none");
    var element2 = document.getElementById("mensajeFormulario");
    element2.classList.remove("d-none");
}
/*Funcion que cambia el color de body*/
function cambiarColor(color) {
    $('body').removeClass("bg-light");
    $('body').addClass(color);
}
